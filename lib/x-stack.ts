import { Stack, StackProps } from "aws-cdk-lib";
import { Construct } from "constructs";
import * as ec2 from "aws-cdk-lib/aws-ec2";
import * as eks from "aws-cdk-lib/aws-eks";
import * as fis from "aws-cdk-lib/aws-fis";
import * as iam from "aws-cdk-lib/aws-iam";
import * as logs from "aws-cdk-lib/aws-logs";

export class XStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const vpc = new ec2.Vpc(this, "Vpc", { maxAzs: 2 });

    const cluster = new eks.Cluster(this, "Cluster", {
      version: eks.KubernetesVersion.V1_21,
      vpc,
      vpcSubnets: [{ subnetType: ec2.SubnetType.PUBLIC }],
      defaultCapacity: 1,
      endpointAccess: eks.EndpointAccess.PUBLIC,
      defaultCapacityInstance: ec2.InstanceType.of(
        ec2.InstanceClass.T3,
        ec2.InstanceSize.MEDIUM
      ),
      clusterLogging: [eks.ClusterLoggingTypes.API],
    });

    cluster.addManifest("chaosmesh-namespace", {
      apiVersion: "v1",
      kind: "Namespace",
      metadata: { name: "chaos-testing" },
    });

    /***
    cluster.addHelmChart("ChaosMesh", {
      repository: "https://charts.chaos-mesh.org",
      chart: "chaos-mesh/chaos-mesh",
      release: "chaos-mesh",
      version: "2.2.2",
      namespace: "chaos-testing",
      createNamespace: true,
    });
    ***/

    const cmManagerRole = {
      kind: "ClusterRole",
      apiVersion: "rbac.authorization.k8s.io/v1",
      metadata: { name: "role-chaosmesh-manager" },
      rules: [
        {
          apiGroups: [""],
          resources: ["pods", "namespaces"],
          verbs: ["get", "watch", "list"],
        },
        {
          apiGroups: ["chaos-mesh.org"],
          resources: ["*"],
          verbs: [
            "get",
            "list",
            "watch",
            "create",
            "delete",
            "patch",
            "update",
          ],
        },
      ],
    };
    cluster.addManifest("chaosmesh-manager-role", cmManagerRole);

    const cmManagerAccount = cluster.addServiceAccount(
      "account-chaosmesh-manager"
    );
    cluster.addManifest("chaosmesh-manager-role-binding", {
      apiVersion: "rbac.authorization.k8s.io/v1",
      kind: "ClusterRoleBinding",
      metadata: { name: "bind-chaosmesh-manager" },
      subjects: [
        {
          kind: "ServiceAccount",
          namespace: "default",
          name: cmManagerAccount.serviceAccountName,
        },
      ],
      roleRef: {
        kind: cmManagerRole.kind,
        name: cmManagerRole.metadata.name,
        apiGroup: "rbac.authorization.k8s.io",
      },
    });

    const fisLog = new logs.LogGroup(this, "FisLogs", { retention: 3 });
    const fisRole = new iam.Role(this, "FisRole", {
      assumedBy: new iam.ServicePrincipal("fis.amazonaws.com"),
      managedPolicies: [
        iam.ManagedPolicy.fromAwsManagedPolicyName("AWSLambdaExecute"),
      ],
      inlinePolicies: {
        ExecutePolicy: new iam.PolicyDocument({
          statements: [
            new iam.PolicyStatement({
              sid: "AllowLogging",
              effect: iam.Effect.ALLOW,
              actions: ["logs:CreateLogDelivery"],
              resources: ["*"],
            }),
          ],
        }),
      },
    });
    cluster.awsAuth.addRoleMapping(fisRole, {
      groups: ["system:masters", cmManagerRole.metadata.name],
    });

    const nginxDeployment = {
      apiVersion: "apps/v1",
      kind: "Deployment",
      metadata: { name: "nginx-deployment" },
      spec: {
        selector: { matchLabels: { app: "nginx" } },
        replicas: 2,
        template: {
          metadata: { labels: { app: "nginx" } },
          spec: {
            containers: [
              {
                name: "nginx",
                image: "nginx:1.14.2",
                ports: [{ containerPort: 80 }],
              },
            ],
          },
        },
      },
    };
    cluster.addManifest("nginx", nginxDeployment);
    new fis.CfnExperimentTemplate(this, "InjectChaosMesh", {
      description: "InjectChaosMesh",
      roleArn: fisRole.roleArn,
      tags: { Name: "InjectChaosMesh" },
      logConfiguration: {
        logSchemaVersion: 1,
        cloudWatchLogsConfiguration: { LogGroupArn: fisLog.logGroupArn },
      },
      targets: {
        cluster: {
          resourceType: "aws:eks:cluster",
          resourceArns: [cluster.clusterArn],
          selectionMode: "ALL",
        },
      },
      actions: {
        InjectChaosMesh: {
          actionId: "aws:eks:inject-kubernetes-custom-resource",
          targets: { Cluster: "cluster" },
          parameters: {
            kubernetesApiVersion: "chaos-mesh.org/v1alpha1",
            kubernetesKind: "PodChaos",
            kubernetesNamespace: "chaos-testing",
            kubernetesSpec: JSON.stringify({
              action: "pod-kill",
              mode: "one",
              selector: {
                namespaces: ["default"],
                labelSelectors: nginxDeployment.spec.template.metadata.labels,
              },
              gracePeriod: 0,
            }),
            maxDuration: "PT5M",
          },
        },
      },
      stopConditions: [{ source: "none" }],
    });
  }
}
